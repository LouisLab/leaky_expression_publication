This numpy array contains the data to create Figure 4C, D and E. 
These plots show that decreasing light intensity from 73uW/mm2 
down to 8uW/mm2 helps with the interpretation of calcium imaging data.
The stimulus duration is 1 second and the stimulus wavelength 590nm. 

Output data is organized the following way:

1st dimension is 600. These are the datapoints 

2nd dimension is 7. These are: Time, Stim, df/f1, df/f2, df/f3, df/f4, df/f5. 
Each df/f is a repeat on the same animal 

3rd dimension is 5, one for each animal 

4th dimension is 3. These are: 8uW/mm2, 47uW/mm2 and 72uW/mm2 

5th dimension is 3. The first contains Gal4+, ATR- data, the second contains Gal4+, ATR+ data
the third contains Gal4-, ATR+

As and example, if one wants to grab Gal4+, ATR+, 8uW/mm2 data the indeces are: 
data[:, 2::,:,0,1].
